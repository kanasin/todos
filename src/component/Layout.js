import React from 'react';
import {
  Grid
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  layout: {
    padding: `10px 10px`,
    margin: `0px auto`
  }
}));

export default (props) => {
  const classes = useStyles();
  return (
    <Grid className={classes.layout} container xs="12" lg="6" direction="row" justify="center" alignItems="center" spacing={3}>
      {props.children}
    </Grid>
  )
}
