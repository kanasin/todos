import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'mobx-react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import LoginPage from './pages/LoginPage'
import TodoPage from './pages/TodoPage'

import allStore from './stores/AllStore';

ReactDOM.render(
  <Provider {...allStore}>
    <React.StrictMode>
      <Router>
        <Switch>
          <Route exact path="/" >
            <LoginPage/>
          </Route>
          <Route path="/todo" >
            <TodoPage/>
          </Route>
        </Switch>
      </Router>
    </React.StrictMode>
  </Provider>,
  document.getElementById('root')
);
