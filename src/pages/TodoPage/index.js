import React, { useState, useEffect } from 'react';
import {
  Grid,
  List,
  Button,
  Modal,
  Backdrop,
  Fade,
  TextField
} from '@material-ui/core';
import SaveIcon from '@material-ui/icons/Save';
import { makeStyles } from '@material-ui/core/styles';
import { observer, inject } from 'mobx-react';
import { withRouter } from 'react-router';
import { toJS, set } from 'mobx';
import axios from 'axios';
import Swal from 'sweetalert2';
import Layout from '../../component/Layout';
import TodoItem from '../../component/TodoItem';

const useStyles = makeStyles((theme) => ({
  center: {
    margin: `0px auto`,
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  space: {
    margin: `20px 0px`
  }
}))

const Page = (props) => {
  const classes = useStyles()
  const { token, loginStatus } = toJS(props.user)
  if(!loginStatus) props.history.push(`/`)
  
  const [open, setOpen] = useState(false)
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [list, setList] = useState([])
  const [editId, setEditId] = useState('')
  const todoItems = list.map(list => <TodoItem key={list.id} edit={() => showModalUpdate(list)} title={list.title} detail={list.description} remove={() => remove(list)} />)
 
  useEffect(() => {
    getAllList()
  }, [])

  const handleOpen = () => {
    setOpen(true);
  }

  const handleClose = () => {
    setOpen(false);
    setTitle('')
    setDescription('')
    setEditId('')
  }

  const createNewItem = async () => {
    try{
      const result = await axios.post(
        `https://candidate.neversitup.com/todo/todos`,
        {
          "title": title,
          "description": description
        },
        { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` } }
      )
      if(result.status === 200){
        setList([...list, result.data])
        setTitle('')
        setDescription('')
        setOpen(false)
        console.log(result.data)
      }
    }catch(e){
      console.log(e)
    }
  }

  const getAllList = async () => {
    try{
      const result = await axios.get(
        `https://candidate.neversitup.com/todo/todos`,
        { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` } }
      )
      if(result.status === 200){
        setList(result.data)
      }
    }catch(e){
      console.log(e)
    }
  }

  const showModalUpdate = (list) => {
    setOpen(true)
    setEditId(list._id)
    setTitle(list.title)
    setDescription(list.description)
  }

  const updateTodo = async () => {
    try{
      const result = await axios.put(
        `https://candidate.neversitup.com/todo/todos/${editId}`,
        {
          "title": title,
          "description": description
        },
        { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` } }
      )
      if(result.status === 200){
        setTitle('')
        setDescription('')
        setEditId('')
        setOpen(false)
        await getAllList()
      }
    }catch(e){
      console.log(e)
    }
  }

  const remove = async (list) => {
    try{
      const confirm = await Swal.fire({
        title: 'Are you sure?',
        text: `Do you want to remove ${list.title}`,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, remove it!'
      })
      if(!confirm.value) return
      const result = await axios.delete(
        `https://candidate.neversitup.com/todo/todos/${list._id}`,
        { headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}` } }
      )
      if(result.status === 200){
        await getAllList()
      }
    }catch(e){
      console.log(e)
    }
  }

  return (
    <Layout xs="12" lg="6" >
      <Grid xs="12" lg="12" >
        <List>
          {todoItems}
        </List>
      </Grid>
      <br/>
        แก้ไขกดที่ไอคอนโฟลเดอร์
      <Grid container item xs="12" lg="12" direction="row" justify="center" alignItems="center" >
        <Button variant="outlined" color="primary" onClick={handleOpen}>
          Create+
        </Button>
        <Modal
          className={classes.modal}
          open={open}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={open}>
            <Grid container item xs={12} lg={12} direction="row" justify="center" alignItems="center">
              <div className={classes.paper}>
                <Grid classes={classes.space} item xs={12} lg={12}>
                  <TextField
                    variant="outlined"
                    required
                    fullWidth
                    id="title"
                    label="Title"
                    name="title"
                    autoComplete="title"
                    value={title}
                    onChange={(e) => setTitle(e.target.value)}
                  />
                </Grid>
                <br/>
                <Grid classes={classes.space} item xs={12} lg={12}>
                  <TextField
                    id="outlined-multiline-static"
                    label="detail"
                    multiline
                    rows={4}
                    value={description}
                    onChange={(e) => setDescription(e.target.value)}
                    variant="outlined"
                  />
                </Grid>
                <br/>
                <Button
                  variant="contained"
                  color="primary"
                  size="small"
                  className={classes.button}
                  startIcon={<SaveIcon />}
                  onClick={() => editId ? updateTodo() : createNewItem()}
                >
                  Save
                </Button>
              </div>
            </Grid>
          </Fade>
        </Modal>
      </Grid>
    </Layout>
  )
}

const pageRouter = withRouter(Page)
export default inject('user')(observer(pageRouter))
