import user from './UserStore';
import bonus from './BonusStore';

export default {
  user,
  bonus
};
